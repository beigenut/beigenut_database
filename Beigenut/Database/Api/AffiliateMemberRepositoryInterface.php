<?php
namespace Beigenut\Database\Api;

interface AffiliateMemberRepositoryInterface
{

    // this is a service class
    /**
     * @return \Beigenut\Database\Api\Data\AffiliateMemberInterface[]
     */
    public function getList();
}