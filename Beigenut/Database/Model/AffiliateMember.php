<?php

namespace Beigenut\Database\Model;

use Magento\Framework\Model\AbstractModel;
use Beigenut\Database\Model\ResourceModel\AffiliateMember as AffiliateMemberResource;
use Beigenut\Database\Api\Data\AffiliateMemberInterface;

class AffiliateMember extends AbstractModel implements AffiliateMemberInterface
{

    protected function _construct()
    {
        // ResourceModel 안에 있는 클라스로 링크 넘겨주기 위해서
        $this->_init(AffiliateMemberResource::class);
    }

    public function getId()
    {
        // Api/Data/AffiliateMember 에서 const 변수 선언한걸 가지고 옴
        // getData("id") 도 가능
        return $this->getData(AffiliateMemberInterface::ID);
    }

    public function getName()
    {
        return $this->getData(AffiliateMemberInterface::NAME);
    }

    public function getStatus()
    {
        return $this->getData(AffiliateMemberInterface::STATUS);
    }

    public function getAddress()
    {
        return $this->getData(AffiliateMemberInterface::ADDRESS);
    }

    public function getPhoneNumber()
    {
        return $this->getData(AffiliateMemberInterface::PHONE_NUMBER);
    }

    public function getCreatedAt()
    {
        return $this->getData(AffiliateMemberInterface::CREATED_AT);
    }

    public function getUpdatedAt()
    {
        return $this->getData(AffiliateMemberInterface::UPDATED_AT);
    }

    public function setId($id)
    {
        return $this->setData(AffiliateMemberInterface::ID);
    }

    public function setName($name)
    {
        return $this->setData(AffiliateMemberInterface::NAME);
    }

    public function setStatus($status)
    {
        return $this->setData(AffiliateMemberInterface::STATUS);
    }

    public function setPhoneNumber($phoneNumber)
    {
        return $this->setData(AffiliateMemberInterface::PHONE_NUMBER);
    }

    public function setAddress($address)
    {
        return $this->setData(AffiliateMemberInterface::ADDRESS);
    }

}