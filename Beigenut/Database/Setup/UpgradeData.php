<?php


namespace Beigenut\Database\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if(version_compare($context->getVersion(), '1.0.2', '<')) {
            $setup->getConnection()->insert(
              $setup->getTable('affiliate_member'),
              ['name'=>'Ade', 'status'=>true, 'address'=>'No. 33, Dubai', 'phone_number'=>'1231231234']
            );
        }

        $setup->endSetup();
    }
}