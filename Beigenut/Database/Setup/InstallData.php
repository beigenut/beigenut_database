<?php


namespace Beigenut\Database\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    public function install(
      ModuleDataSetupInterface $setup,
      ModuleContextInterface $context
    ) {
//        $setup->startSetup();

        $data = [
          ['name' => 'bob', 'address' => 'No. 10, Dubai', 'status' => true],
          ['name' => 'alex', 'address' => 'No. 22, Dubai'],
        ];

        foreach ($data as $bind) {
            $setup->getConnection()
              ->insertForce($setup->getTable('affiliate_member'), $bind);
        }

        //        $setup->getConnection()->insert(
        //            $setup->getTable('affiliate_member'),
        //            ['name'=>'bob', 'address'=>'No. 10, Dubai', 'status'=>true]
        //          );
        //
        //        $setup->getConnection()->insert(
        //          $setup->getTable('affiliate_member'),
        //          ['name'=>'alex', 'address'=>'No. 22, Dubai']
        //        );

//        $setup->endSetup();
    }

}